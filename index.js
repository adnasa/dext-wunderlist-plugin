const request = require('request')

module.exports = {
  keyword: 'wunderlist',
  action: undefined,
  helper: {
    titie: 'Wunderlist',
    subtitle: 'manage your tasks',
  },
  execute: q => {
    const phrases = {
      create: /new\: (.*)/,
    }

    const wunderlistAction = Object.keys(phrases).reduce((acc, action) => {
      const phrase = phrases[action]
      const match = q.match(phrase)
      if (match) {
        [action, title] = match
        return {
          action,
          title,
          context: match.input,
        }
      }
      return acc
    }, {})

    return { items: [] }
  },
}
